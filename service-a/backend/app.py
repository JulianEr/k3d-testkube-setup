from flask import Flask, Blueprint
from datetime import datetime

app = Flask(__name__)

@app.route('/datetime', methods=['GET'])
def hello_world():
    return datetime.now().strftime("%d-%m-%Y %H:%M:%S")