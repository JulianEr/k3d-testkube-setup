This project is thought to be an example on how to set up testkube for a k3d environment. Needless to say that to make this project run you need both technologies. You can make your life easier if you use [Chocolatey](https://chocolatey.org/install). With Chocolatey you can install [k3d](https://k3d.io) simply with `choco install k3d`. Installing [tetskube](https://docs.testkube.io/getting-started/step1-installing-cli/) is almost as simple as installing k3d with Chocolatey. It is executing the following commands:

```
choco source add --name=kubeshop_repo --source=https://chocolatey.kubeshop.io/chocolatey  
choco install testkube -y
```

Before you can set up the cluster you need to create a new local docker registry for our deployments. To [create](https://k3d.io/v5.2.1/usage/commands/k3d_registry_create/) the registry use 

```
k3d registry create testkube-setup-registry.localhost --port 30050
```

If you want to get [rid](https://k3d.io/v5.2.1/usage/commands/k3d_registry_delete/) of the registry just execute

```
k3d registry delete testkube-setup-registry.localhost
```

With the registry up and running we can create the cluster with the config file in `cluster/` (assuming you are in the projects root directory aka the directory this file is). [Creating](https://k3d.io/v5.2.1/usage/commands/k3d_cluster_create/) the cluster is as simple as executing

```
k3d cluster create --config cluster\config.yml
```

and if you want to [delete](https://k3d.io/v5.2.1/usage/commands/k3d_cluster_delete/) the cluster again just hit 

```
k3d cluster delete testkube-setup
```

Before deploying our services let us set up the dashboard following this [guide](). First execute the yaml file with 

```
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.7.0/aio/deploy/recommended.yaml
```

and then create the admin user and the needed secrets with (again from the root folder)

```
kubectl apply -k cluster\dashboard-setup\
```

You can access the dashboard by executing

```
kubectl proxy
```

and go to the dashboard [here](http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/). To access the dashboard you need the token of the secret we just created for the admin user. To easily copy it into Windows clipboard just use

```
kubectl -n kubernetes-dashboard describe secret admin-user-token | sed -n -e 's/^token:\s*\(\S\+\)/\1/p' | clip
```

For Linux the command should be pretty similar just use `xclip` instead of `clip` assuming it is installed on your system.
Remember it is just in your clipboard so you just need to `Ctrl+v` it into the field for the token. Access the UI to verify the setup work and then we can start building and deploying our apps.
Let us start with creating the namespace for the apps by executing

```
kubectl apply -f cluster/cluster-setup/namespace.yaml
```

Finally we start to deploy the service a by building the image with (there are probably more elegant ways not using ´--no-cache´ but for simplicity let us stick with this approach, otherwise on each rebuild, after changing the app the push will not do a thing)

```
docker build -t localhost:30050/service-a:latest service-a/ --no-cache
```

and push it to the registry with

```
docker push localhost:30050/service-a:latest
```

As `service-b` is a Java-Web App we need to first build it with 

```
mvn -f .\service-b\pom.xml clean install
```

and build and push the image similar to `service-a` with 

```
docker build -t localhost:30050/service-b:latest service-b/ --no-cache
docker push localhost:30050/service-b:latest
```

With both images in the registry we can deploy both services into the cluster with

```
kubectl apply -k service-a/kustomize/base/
kubectl apply -k service-b/kustomize/base/
```

When the pods are up and running you can see both services communicating when visiting the exposed port on your localhost on the `time` endpoint [here](http://localhost:30100/time).
To make testkube available run 

```
testkube init
```

And verify that your context is `k3d-testkube-setup` and confirm. When the installation is done enter the dashboard with

```
testkube dashboard
```

Which is opening the dashboard for you.