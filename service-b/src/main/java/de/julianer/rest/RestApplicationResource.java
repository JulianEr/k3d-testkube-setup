package de.julianer.rest;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.eclipse.microprofile.rest.client.inject.RestClient;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

@ApplicationScoped
@Path("/time")
public class RestApplicationResource {
    
    @Inject
    @RestClient
    private RestApplicationClient client;

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getTimeTillTomorrow() {
        final String fetchedTime = client.get();
        final LocalDateTime date = LocalDateTime.parse(fetchedTime, DateTimeFormatter.ofPattern("dd-MM-yyy HH:mm:ss"));
        final LocalDateTime tomorrow = date.plusDays(1).toLocalDate().atStartOfDay();
        final Duration tillTomorrow = Duration.between(date, tomorrow);
        return String.format("Es wird in %d Stunden, %d Minuten und %d Sekunden Mitternacht sein", tillTomorrow.toHours() % 24L, tillTomorrow.toMinutes() % 60L, tillTomorrow.toSeconds() % 60L);
    }
}
