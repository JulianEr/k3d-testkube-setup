package de.julianer.rest;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import jakarta.ws.rs.Produces;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.MediaType;

@RegisterRestClient(configKey = "restClient", baseUri = "http://service-a.time-app.svc.cluster.local:5000")
@Path("/datetime")
public interface RestApplicationClient extends AutoCloseable {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String get();

}
